# Spring Boot Projects

## Technology
- Spring Boot
- REST Api

## Tools
- Spring Tool Suite

## 1. Course Api Data
This is simple spring boot project which have data operations.

## Description
This project have table operations of Topics and Courses. The rest mappings which is created with POST, GET, DELETE, PUT methods have used in this application. I have used Apache Derby dependency to create a local table for inserting. And I have used CrudRepository for the crud operations on Topics and Courses.

## 2. Simple Basic Auth
This is the simple basic authentication project.

## Description
In this project, I have used Spring security dependency to do basic authentication. I have used 2 mapping which is authenticate in the Config class. I have used WebSecurityConfigurerAdapter in the config class and override 2 configure methods. I have used In-Memory authentication to hardcore all the user details such as roles, passwords, and the user name. And assigned authentication by a particular role to each mappings.

