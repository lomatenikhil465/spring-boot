package com.basicauth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@GetMapping("/insert")
	public String insert() {
		return "This is insert request";
	}
	
	@GetMapping("/details")
	public String details() {
		return "This is details request";
	}
	
}
