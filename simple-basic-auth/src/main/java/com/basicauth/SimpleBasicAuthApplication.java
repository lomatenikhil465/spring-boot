package com.basicauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleBasicAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleBasicAuthApplication.class, args);
	}

}
